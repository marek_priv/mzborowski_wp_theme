<!DOCTYPE html>
<html>
    <head>
    	<meta name="viewport" content="width=device-width, user-scalable=no">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta charset="<?php bloginfo(charset); ?>">
		<title><?php bloginfo(blogname); ?></title>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
		<link href="<?php bloginfo(stylesheet_url); ?>" rel="stylesheet" type="text/css">
		<link href="<?php bloginfo(template_directory); ?>/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="<?php bloginfo(template_directory); ?>/lib/jquery/jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="<?php bloginfo(template_directory); ?>/lib/bootstrap/js/bootstrap.min.js"></script>
		<?php wp_head(); ?>
	</head>
	<body>