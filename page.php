<?php get_header(); ?>


<!-- BODY start -->

    
<div class="main-wrapper container-fluid">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<div class="entry pageId-<?php echo get_the_ID(); ?>">
			<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

			<?php the_content('Czytaj dalej...'); ?>
			<br class="clear" />
		</div> <!-- end entry -->

	<?php endwhile; else: ?>
		<p><?php _e('Nie znaleziono postów spełniających podane kryteria.'); ?></p>	
	<?php endif; ?>
    
</div>


<!-- BODY start -->



<?php //get_sidebar(); ?>




<?php get_footer(); ?>
